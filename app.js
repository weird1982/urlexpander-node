const express = require('express');
const app = express();
const fetch = require('node-fetch');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

/*
    const cors = require('cors')
    app.use(cors())
 */

app.put('/api/expand', async (req, res) => {
    let url
    try {
        url = new URL(req.body.url)
    }
    catch (e) {
        res.status(404)
        res.end(JSON.stringify({ status: 1,url:"" }));
        return
    }

    if (['vm.tiktok.com', 'soundcloud.app.goo.gl', 'youtube.com'].includes(url.host)){
        const resp = await fetch(url, {method: 'HEAD'})
        res.setHeader('Content-Type', 'application/json');
        if (resp.status === 200) return res.end(JSON.stringify({status: 0, url: resp.url}));
    }
        res.status(404)
        res.end(JSON.stringify({status: 1, url: ""}));

});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
